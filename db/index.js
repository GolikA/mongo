const MongoClient = require('mongodb').MongoClient;
const {uri, db} = require('./config')



const client = new MongoClient(uri, { useNewUrlParser: true });
client.connect(err => {
    const collection = client.db("test").collection("devices");
    // perform actions on the collection object
    console.log('DB is connected')
    client.close();
});
